from story import Story

# temporary import
from player import Player
from monster import Monster

from sys import argv
from sys import exit

def Verify_Int(player_input):
	ret_val = -1

	try:
		# try to convert to int
		ret_val = int(player_input)
	except:
		print "Bad Input: " + player_input

	return ret_val


def Combat_Mode(player, monster):
	print "Initiating Combat Mode. Engaging " + monster.name + ".\n"

	# TODO: make it interesting and random number generator turn 1-2
	turn = 0

	# make a copy of the health to maintain monster obj
	monster_health = monster.HP 

	while(1):
		if monster_health <= 0:
			print "Victory: " + player.name + " has defeated the " + monster.name + ".\n"
			break

		if player.HP <= 0:
			exit("Defeat: " + player.name + " has been slain by the " + monster.name) + ".\n"

		# print current state details
		print "Stats:"
		print player.name + " HP: " + str(player.HP)
		print monster.name + " HP: " + str(monster_health) + "\n"

		if turn%2 == 0:
			# player's turn
			player.Print_Options()
			while(1):
				player_input = raw_input("\nSelect Attack: ")
				idx = Verify_Int(player_input)
				if idx >= 0:
					dmg = player.Get_Dmg(idx)
					if dmg != None:
						monster_health -= dmg
						# interface the game
						print player.name + " used " + player.attack_name[idx] + "."
						print monster.name + " has " + str(monster_health) + " HP remaining.\n"
						break
		else:
			# monster's turn
			# TODO: incorporate use of different attacks
			idx = 0
			dmg = monster.AP[idx]
			player.HP -= dmg
			print monster.name + " used " + monster.attack_name[idx] + "."
			print player.name + " has " + str(player.HP) + " HP remaining.\n"

		turn += 1

	return


# Execution of the game
def Play(Adventure):
	print "Playing: " + Adventure.game_name

	curr_event_name = Adventure.start_event
	curr_event = Adventure.event_selector[curr_event_name]

	minion = Monster("Umang", 40, {"stare": 2})
	you = Player("Jerry", 30, {"swipe": 4, "kick": 5, "name call": 2})

	while(1):
		print "Event: " + curr_event.event_name + "\n"
		
		# temporary test, will need to make a hard copy
		m = minion
		Combat_Mode(you, m)
		# end temporary test

		print curr_event.event_situation + "\n"
		
		if len(curr_event.transitions) == 0:
			break

		curr_event.Print_Options()

		# Loop til valid input
		while(1):
			player_input = raw_input("\nEnter Option: ")
			idx = Verify_Int(player_input)
			if idx >= 0:
				curr_event_name = curr_event.Get_Next_Event(idx)
				if len(curr_event_name) > 0: # valid room
					break

		curr_event = Adventure.event_selector[curr_event_name]

	return


def main():
	if len(argv) != 2:
		exit("Not enough arguments.")

	file = argv[1]

	game = Story(file)
	Play(game)

	return

if __name__ == '__main__':
	main()
